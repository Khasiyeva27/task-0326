import java.util.Scanner;

public class Eolymp8956 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];

        int count = 0;
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();

            if (arr[i] < 0) {
                count++;
            }
        }
        if (count != 0) {
            System.out.println(count);
            for (int j = n-1; j >=0; j--) {
                if (arr[j] < 0) {
                    System.out.print(arr[j] + " ");
                }
            }
        } else {
            System.out.println("NO");
        }
    }
}

