import java.util.Scanner;

public class Eolymp8961 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int [] arr=new int[n];

        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();
        }

        int imin=0;

        for(int i=1;i<n;i++){
            if(arr[i]<arr[imin]){
                imin=i;
            }
        }
        int temp=arr[0];

        arr[0]=arr[imin];
        arr[imin]=temp;

        for(int i=0;i<n;i++){
            System.out.print(arr[i]+" ");
        }
    }
}
