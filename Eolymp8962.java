import java.util.Scanner;

public class Eolymp8962 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int imax = n - 1;
        for (int j = n - 2; j >= 0; j--) {
            if (arr[j] > arr[imax]) {
                imax = j;
            }
        }

        int temp = arr[n - 1];
        arr[n - 1] = arr[imax];
        arr[imax] = temp;

        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}