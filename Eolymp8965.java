import java.util.Scanner;

public class Eolymp8965 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int [] arr=new int[n];

        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();
        }

        int min=arr[0];
        for(int j=1;j<n;j++){
            if(arr[j]<min){
                min=arr[j];
            }
        }

        int premin=min/2;
        for(int k=0;k<n;k++){
            System.out.print(arr[k]-premin+" ");
        }
    }
}
